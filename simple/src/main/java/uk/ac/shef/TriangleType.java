package uk.ac.shef;

public enum TriangleType {
	INVALID, SCALENE, EQUILATERAL, ISOSCELES
}
