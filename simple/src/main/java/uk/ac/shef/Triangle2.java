package uk.ac.shef;

public class Triangle2 {

    public static TriangleType classify(int a, int b, int c)
    {
        if (a <= 0 || b <= 0 || c <= 0) {
            return TriangleType.INVALID;
        }
        if (! (a + b > c && a + c > b && b + c > a)) {
            return TriangleType.INVALID;
        }
        if (a == b && b == c) {
            return TriangleType.EQUILATERAL;
        }
        if (a == b || b == c || a == c) {
            return TriangleType.ISOSCELES;
        }
        return TriangleType.SCALENE;
    }
}
